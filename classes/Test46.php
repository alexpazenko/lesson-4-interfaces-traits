<?php

class Test46
{
    use Trait1, Trait2, Trait3 {
        Trait1::method insteadof Trait2;
        Trait2::method insteadof Trait3;
        Trait2::method as method22;
        Trait3::method as method23;
    }

    public function getSum()
    {
        return $this->method1() + $this->method2() + $this->method3();
    }

    public function getSum2()
    {
        return $this->method() + $this->method22() + $this->method23();
    }
}