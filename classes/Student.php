<?php

class Student extends User
{
    private $course;
    private $scholarship;

    public function addOneYear()
    {
        $this->age++;
    }

    public function getCourse()
    {
        return$this->course;
    }

    public function setScholarship(int $scholarship)
    {
        $this->scholarship = $scholarship;
    }
    public function getScholarship()
    {
        return$this->scholarship;
    }

    public function setCourse(int $course)
    {
        $this->course = $course;
    }

    public function setName(string $name)
    {
        if (strlen($name) < 10) {
        parent::setName($name);
        }
    }
}