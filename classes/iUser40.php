<?php

interface iUser40
{
    public function getName();
    public function setName($name);
    public function getAge();
    public function setAge($age);
}