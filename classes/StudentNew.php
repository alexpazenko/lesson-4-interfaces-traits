<?php

class StudentNew extends UserNew
{
    private $course;

    public function __construct( string $name, int $age, int $course)
    {
        parent::__construct($name, $age);
        $this->course = $course;
    }

    public function getCourse()
    {
        return $this->course;
    }
}