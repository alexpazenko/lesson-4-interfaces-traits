<?php


class UsersCollection
{
    private $employees = [];
    private $students = [];

    public function add($user)
    {
        if ($user instanceof Employee) {
            $this->employees[] = $user;
        }

        if ($user instanceof Student) {
            $this->students[] = $user;
        }
    }

    public function getTotalSalary()
    {
        $totalSalary = 0;
        foreach($this->employees as $employee) {
            $totalSalary += $employee->getSalary();
        }
        return $totalSalary;
    }

    public function getTotalScholarship()
    {
        $totalScholarship = 0;
        foreach($this->students as $student) {
            $totalScholarship += $student->getScholarship();
        }
        return $totalScholarship;
    }

    public function getTotalPayment()
    {
        return $this->getTotalSalary() + $this->getTotalScholarship();
    }

}