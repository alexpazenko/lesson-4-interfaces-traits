<?php

class Student35 extends User35
{
    private $scholarship;

    public function getScholarship()
    {
        return $this->scholarship;
    }

    public function setScholarship($scholarship)
    {
        $this->scholarship = $scholarship;
    }

    public function increaseRevenue($value)
    {
        $this->scholarship = $this->scholarship + $value;
    }

    public function decreaseRevenue($value)
    {
        $this->scholarship = $this->scholarship - $value;
    }
}