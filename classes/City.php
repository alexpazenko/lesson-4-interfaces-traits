<?php

class City
{
    private $name;
    private $population;

    public function setName ($name)
    {
        $this->name = $name;
    }

    public function setPopulation ($population)
    {
        $this->population = $population;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPopulation()
    {
        return $this->population;
    }
}