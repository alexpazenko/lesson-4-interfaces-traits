<?php

class Driver extends Employee
{
    private $drivingExperience;
    private $drivingCategory = [];

    public function setDrivingExperience(int $drivingExperience)
    {
        $this->drivingExperience = $drivingExperience;
    }
    public function getDrivingExperience()
    {
        return $this->drivingExperience;
    }

    public function setDrivingCategory(array $drivingCategory)
    {
        $this->drivingCategory = array_merge($this->drivingCategory, $drivingCategory);
    }
    public function getDrivingCategories()
    {
        return $this->drivingCategory;
    }
}