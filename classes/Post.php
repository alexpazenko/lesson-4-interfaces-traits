<?php

class Post
{
    private $post;
    private $salary;

    public function __construct(string $post, int $salary)
    {
        $this->post = $post;
        $this->salary = $salary;
    }

    public function getPost()
    {
        return $this->post;
    }

    public function getSalary()
    {
        return $this->salary;
    }
}