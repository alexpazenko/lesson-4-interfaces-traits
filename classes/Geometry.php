<?php

class Geometry
{
    private static $pi =  3.14;

    public static function getCircleSquare(int $radius)
    {
        return self::$pi * $radius * $radius;
    }

    public static function getCircleСircuit(int $radius)
    {
        return 2 * self::$pi * $radius;
    }

    public static function getSphereVolume(int $radius)
    {
        return (4/3) * self::$pi * ($radius * $radius * $radius);
    }
}