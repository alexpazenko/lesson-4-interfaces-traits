<?php

class Programmer43 extends Employee43 implements iProgrammer
{
    private $lang = [];

    public function getLangs()
    {
        return $this->lang;
    }
    public function addLang($lang)
    {
        $this->lang[] = $lang;
    }
}