<?php

class Sphere implements iSphere
{
    private $radius;
    public function __construct($radius)
    {
        $this->radius = $radius;
    }
    public function getVolume()
    {
        return $this->radius;
    }
    public function getSquare()
    {
        return $this->radius;
    }
}