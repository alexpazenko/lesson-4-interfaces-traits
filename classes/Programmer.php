<?php

class Programmer extends Employee
{
    private $langs = [];

    public function addLanguage($language)
    {
        array_push($this->langs, $language);
    }

    public function getLangs()
    {
        return $this->langs;
    }
}