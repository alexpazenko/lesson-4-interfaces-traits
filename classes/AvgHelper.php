<?php

class AvgHelper
{
    private $sumHelper;

    public function __construct()
    {
        $this->sumHelper = new SumHelper();
    }
    public function getAvg($arr)
    {
        return $this->average($arr);
    }

    public function getMeanSquare($arr)
    {
        return $this->calcSqrt($this->sumHelper->getSum2($arr), 2);
    }

    public function getAvgMean($arr)
    {
        return $this->calcSqrt($this->average($arr), 2);
    }

    private function average($arr)
    {
        return array_sum($arr) / count($arr);
    }

    private function calcSqrt($num, $power)
    {
        return pow($num, 1/$power);
    }
}