<?php

class Quadrate implements iFigure
{
    private $side;

    public function __construct($side)
    {
        $this->side = $side;
    }

    public function getSquare()
    {
        return $this->side * $this->side;
    }

    public function getPerimeter()
    {
        return 4 * $this->side;
    }
}