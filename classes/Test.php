<?php

class Test
{
    public $prop1;
    public $prop2;
    private $prop3;
    private $prop4;

    /*public function __construct()
    {
        $testVars =  get_class_vars('Test');
        foreach ($testVars as $name => $value)
        {
            echo 'Method: ' . $name . '<br>';
        }
    }*/

    public function method1()
    {
        return 'Это method1';
    }

    public function method2()
    {
        return 'Это method2';
    }

    public function method3()
    {
        return 'Это method3';
    }
}