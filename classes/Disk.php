<?php

class Disk implements iFigure
{
    private $radius;
    const PI = 3.14;

    public function __construct($radius)
    {
        $this->radius = $radius;
    }

    public function getSquare()
    {
        return self::PI * ($this->radius * $this->radius);
    }

    public function getPerimeter()
    {
        return 2 * $this->radius * self::PI;
    }
}