<?php

class EmployeeNew extends UserNew
{
    private $birthday;
    private $age;
    private $salary;

    public function __construct( string $name, int $age, string $birthday, int $salary) {

        parent::__construct($name, $age);
        $this->birthday = $birthday;
        $this->age = $this->calculateAge($birthday);
        $this->salary = $salary;
    }

    private function calculateAge($birthday) {
       $then = date('Ymd', strtotime($birthday));
       $diff = date('Ymd') - $then;
        return substr($diff, 0, -4);
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getSalary()
    {
        return $this->salary;
    }
}