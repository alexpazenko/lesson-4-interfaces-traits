<?php

class Circle
{
    const PI = 3.14;
    private $radius;

    public function __construct($radius)
    {
        $this->radius = $radius;
    }

    public function getSquare()
    {
        // Пи умножить на квадрат радиуса
        return self::PI * ($this->radius * $this->radius);
    }

    public function getCircuit()
    {
        // 2 Пи умножить на радиус
        return self::PI * 2 * $this->radius;
    }
}