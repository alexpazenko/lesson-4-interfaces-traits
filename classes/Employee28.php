<?php

class Employee28
{
    private $name;
    private $surname;
    private $post;

    public function __construct(string $name, string $surname, Post $post)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->post = $post;
    }

    public function setName ($name)
    {
        $this->name = $name;
    }

    public function getName ()
    {
       return $this->name;
    }

    public function setSurname ($surname)
    {
        $this->surname = $surname;
    }

    public function getSurname ()
    {
        return $this->surname;
    }

    public function getPost ()
    {
        return $this->post;
    }

    public function changePost (Post $post)
    {
        $this->post = $post;
    }
}