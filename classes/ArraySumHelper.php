<?php

class ArraySumHelper
{
    public static function getSum2($arr)
    {
        return self::getSum($arr, 2);
    }

    public static function getSum3($arr)
    {
        return self::getSum($arr, 3);
    }

    private static function getSum(array $arr, int $power)
    {
        $sum = 0;
        foreach ($arr as $number) {
            $sum += pow($number, $power);
        }
        return $sum;
    }
}