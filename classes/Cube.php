<?php

class Cube implements iCube
{
    private $side;

    public function __construct($side)
    {
        $this->side = $side;
    }

    public function getСubeVolume()
    {
        return $this->side * $this->side * $this->side;
    }

    public function getСubeSurface()
    {
        return 6 * ($this->side * $this->side);
    }
}