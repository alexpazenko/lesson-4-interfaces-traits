<?php

class Arr
{
    private $nums = [];
    private $sumHelper;
    private $avgHelper;

    public function __construct()
    {
        $this->sumHelper = new SumHelper();
        $this->avgHelper = new AvgHelper();
    }
    public function getSum23()
    {
        $nums = $this->nums;
        return $this->sumHelper->getSum2($nums) + $this->sumHelper->getSum3($nums);
    }

    public function add($number)
    {
        $this->nums[] = $number;
    }
    public function getAvgMeanSum()
    {
        $nums = $this->nums;
        return $this->avgHelper->getAvg($nums) + $this->avgHelper->getMeanSquare($nums);
    }
}