<?php

class Test51
{
    use Trait51_2;

    public function getSum()
    {
       return $this->method1() + $this->method2() + $this->method3();
    }
}