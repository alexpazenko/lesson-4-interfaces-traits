<?php

class Cube41 implements iFigure3d
{
    private $side;

    public function __construct($side)
    {
        $this->side = $side;
    }

    public function getVolume()
    {
        return $this->side * $this->side * $this->side;
    }

    public function getSurfaceSquare()
    {
        return 6 * ($this->side * $this->side);
    }
}