<?php

require_once 'classes/User.php';
require_once 'classes/Employee.php';
require_once 'classes/Student.php';
require_once 'classes/Programmer.php';
require_once 'classes/Driver.php';
require_once 'classes/UserNew.php';
require_once 'classes/StudentNew.php';
require_once 'classes/EmployeeNew.php';
require_once 'classes/Product.php';
require_once 'classes/Arr.php';
require_once 'classes/SumHelper.php';
require_once 'classes/AvgHelper.php';
require_once 'classes/City.php';
require_once 'classes/UsersCollection.php';
require_once 'classes/Post.php';
require_once 'classes/Employee28.php';
require_once 'classes/ArraySumHelper.php';
require_once 'classes/Num.php';
require_once 'classes/Geometry.php';
require_once 'classes/User31.php';
require_once 'classes/Circle.php';
require_once 'classes/Test1.php';
require_once 'classes/Test2.php';
require_once 'classes/Test.php';
require_once 'classes/Test3.php';
require_once 'classes/User35.php';
require_once 'classes/Employee35.php';
require_once 'classes/Student35.php';
require_once 'classes/Figure.php';
require_once 'classes/Rectangle.php';
require_once 'classes/iFigure.php';
require_once 'classes/Disk.php';
require_once 'classes/iTetragon.php';
require_once 'classes/iFigure3d.php';
require_once 'classes/Quadrate.php';
require_once 'classes/Cube41.php';
require_once 'classes/Rectangle42.php';
require_once 'classes/iCircle.php';
require_once 'classes/Disk42.php';
require_once 'classes/iProgrammer.php';
require_once 'classes/Employee43.php';
require_once 'classes/iSphere.php';
require_once 'classes/Sphere.php';
require_once 'classes/iTest1.php';
require_once 'classes/Helper.php';
require_once 'classes/Country.php';
require_once 'classes/Trait1.php';
require_once 'classes/Trait2.php';
require_once 'classes/Trait3.php';
require_once 'classes/Test46.php';
require_once 'classes/Trait51_1.php';
require_once 'classes/Trait51_2.php';
require_once 'classes/Test51.php';


// Exercise 1 - Сделайте класс Programmer, который будет наследовать от класса Employee.
// Пусть новый класс имеет свойство langs, в котором будет хранится массив языков, которыми владеет программист.
// Сделайте также геттер и сеттер для этого свойства.
$programmer = new Programmer();
$programmer->setName('john');
$programmer->setAge(25);
$programmer->setSalary(1000);
$programmer->addLanguage('php');
$programmer->addLanguage('javascript');

echo $programmer->getName() . "<br>";
echo $programmer->getAge() . "<br>";
echo $programmer->getSalary() . "<br>";
echo 'Languages: ';
foreach ($programmer->getLangs() as $language) {
    echo $language. ' ';
}
echo "<br>";

// Exercise 2 - Сделайте класс Driver (водитель), который будет наследовать от класса Employee.
// Пусть новый класс добавляет следующие свойства: водительский стаж, категория вождения (A, B, C, D),
// а также геттеры и сеттеры к ним.
$driver = new Driver;
$driver->setName('bobby');
$driver->setAge(45);
$driver->setDrivingExperience(18);
$driver->setDrivingCategory(['A','B','C']);
echo $driver->getName() . "<br>";
echo $driver->getAge() . "<br>";
echo 'Categories: ';
foreach ($driver->getDrivingCategories() as $language) {
    echo $language . ' ';
}
echo "<br>";

// Exercise 3 - Реализуйте описанный класс Student с методами getCourse, setCourse и addOneYear.
$student = new Student();
$student->setName('bill');
$student->setCourse(3);
$student->setAge(21);
echo   $student->getCourse()."<br>";
$student->addOneYear();
echo $student->getAge();
echo "<br>";

// Exercise 4 - Добавьте в класс Student метод setName, в котором будет выполняться проверка того,
// что длина имени более 3-х символов и менее 10 символов.
// Пусть этот метод использует метод setName своего родителя, чтобы выполнить часть проверки.
$student2 = new Student();
$student2->setName('bill');
$student2->setName('bob');
echo $student2->getName(); //will return just name bill not bob
echo "<br>";

// Exercise 5 - Модифицируйте так, чтобы третьим параметром в конструктор передавалась
// дата рождения работника в формате год-месяц-день. Запишите ее в свойство birthday.
// Сделайте геттер для этого свойства.
// Exercise 6 - Модифицируйте предыдущую задачу так, чтобы метод calculateAge вызывался в конструкторе объекта,
// рассчитывал возраст пользователя и записывал его в приватное свойство age. Сделайте геттер для этого свойства.
// Exercise 7 - Сделайте класс Employee, который будет наследовать от класса User.
// Пусть новый класс имеет свойство salary, в котором будет хранится зарплата работника.
// Зарплата должна передаваться четвертым параметром в конструктор объекта.
// Сделайте также геттер для этого свойства.
$employeeNew = new EmployeeNew('Alex',50, '2000-12-01', 1000);
echo $employeeNew->getBirthday(). "<br>";
echo $employeeNew->getAge(). "<br>";
echo $employeeNew->getSalary(). "<br>";

// Exercise 8 - Сделайте класс Product, в котором будут следующие свойства: name, price.
// Exercise 9 - Создайте объект класса Product, запишите его в переменную $product1.
// Exercise 10 - Присвойте значение переменной $product1 в переменную $product2.
// Проверьте то, что обе переменные ссылаются на один и тот же объект.
$product1 = new Product('boots', 1000);
$product2 = $product1;
echo $product1->name . '<br>';
echo $product2->price . '<br>';

// Exercise 24-1 - Реализуйте классы Arr и SumHelper.
$arr = new Arr();
$arr->add(15);
$arr->add(30);
$arr->add(2);

echo $arr->getSum23() . '<br>';

// Exercise 24-2 - Создайте класс AvgHelper с методом getAvg, который параметром будет принимать массив
// и возвращать среднее арифметическое этого массива (сумма элементов делить на количество).
$avgHelper = new AvgHelper();
$arr = range(1, 5);
echo $avgHelper->getAvg($arr) . '<br>';


// Exercise 24-3 - Добавьте в класс AvgHelper еще и метод getMeanSquare, который параметром
// будет принимать массив и возвращать среднее квадратичное этого массива
// (квадратный корень, извлеченный из суммы квадратов элементов, деленной на количество).
$avgHelper = new AvgHelper();
$arr = range(1, 2);
echo $avgHelper->getMeanSquare($arr) . '<br>';


// Exercise 24-4 - Добавьте в класс Arr метод getAvgMeanSum, который будет находить сумму среднего арифметического
// и среднего квадратичного из массива $this->nums.
$arrGetAvgMeanSum = new Arr();
$arrGetAvgMeanSum->add(1);
$arrGetAvgMeanSum->add(2);
$arrGetAvgMeanSum->add(3);

echo $arrGetAvgMeanSum->getAvgMeanSum() . '<br>';

// Exercise 27-3 - Создайте по 3 объекта каждого класса (Employee, Student)
// и в произвольном порядке запишите их в массив $arr.
$employee1 = new Employee();
$employee1->setName('James');
$employee1->setSalary(1000);
$employee2 = new Employee();
$employee2->setName('Robert');
$employee2->setSalary(1200);
$employee3 = new Employee();
$employee3->setName('John');
$employee3->setSalary(700);

$student1 = new Student();
$student1->setName('William');
$student1->setScholarship(380);
$student2 = new Student();
$student2->setName('David');
$student2->setScholarship(350);
$student3 = new Student();
$student3->setName('Joseph');
$student3->setScholarship(420);

$arr = [];
$arr[] = $employee1;
$arr[] = $student2;
$arr[] = $student3;
$arr[] = $employee3;
$arr[] = $student1;
$arr[] = $employee2;


// Exercise 27-4 - Переберите циклом массив $arr и выведите на экран столбец имен всех работников.
echo 'Список работников: <br>';
foreach ($arr as $object)
{
    if ($object instanceof Employee) {
        echo $object->getName() . '<br>';
    }
}

// Exercise 27-5 - Аналогичным образом выведите на экран столбец имен всех студентов.
echo 'Список студентов: <br>';
foreach ($arr as $object)
{
    if ($object instanceof Student) {
        echo $object->getName() . '<br>';
    }
}

// Exercise 27-6 - Переберите циклом массив $arr и с его помощью найдите сумму зарплат работников
// и сумму стипендий студентов. После цикла выведите эти два числа на экран.
$salarySum = 0;
$scholarshipSum = 0;
foreach ($arr as $object)
{
    if ($object instanceof Employee) {
        $salarySum += $object->getSalary();
    } elseif ($object instanceof Student) {
        $scholarshipSum += $object->getScholarship();
    }
}

echo 'Сумма зарплат работников: ' . $salarySum . '<br>';
echo 'Сумма стипендий студентов: ' . $scholarshipSum . '<br>';

// Exercise 27-10 - Создайте 3 объекта класса User, 3 объекта класса Employee,
// 3 объекта класса City, и в произвольном порядке запишите их в массив $arr.

$user1 = new User();
$user1->setName('Matthew');
$user2 = new User();
$user2->setName('Steven');
$user3 = new User();
$user3->setName('Paul');

$employee1 = new Employee();
$employee1->setName('James');
$employee2 = new Employee();
$employee2->setName('Robert');
$employee3 = new Employee();
$employee3->setName('John');

$city1 = new City();
$city1->setName('New York');
$city2 = new City();
$city2->setName('Los Angeles');
$city3 = new City();
$city3->setName('Chicago');

$arr = [];
$arr[] = $city3;
$arr[] = $user1;
$arr[] = $user3;
$arr[] = $employee2;
$arr[] = $city1;
$arr[] = $employee1;
$arr[] = $employee3;
$arr[] = $user2;
$arr[] = $city2;


// Exercise 27-11 - Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов,
// которые принадлежат классу User или потомку этого класса.
echo 'Принадлежат классу User или потомку этого класса: <br>';
foreach ($arr as $object)
{
    if ($object instanceof User) {
        echo $object->getName() . '<br>';
    }
}

// Exercise 27-12 - Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов,
// которые не принадлежат классу User или потомку этого класса.
echo 'Не принадлежат классу User или потомку этого класса: <br>';
foreach ($arr as $object)
{
    if ($object instanceof City) {
        echo $object->getName() . '<br>';
    }
}

// Exercise 27-13 - Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов,
// которые принадлежат именно классу User, то есть не классу City и не классу Employee.
echo 'Принадлежат именно классу User: <br>';
foreach ($arr as $object)
{
    if (!$object instanceof Employee && !$object instanceof City ) {
        echo $object->getName() . '<br>';
    }
}

// Exercise 27-14 - реализуйте класс UsersCollection.

$usersCollection = new UsersCollection();
$student1 = new Student();
$student1->setName('Donald');
$student1->setScholarship('100');
$student2 = new Student();
$student2->setName('Kevin');
$student2->setScholarship('200');
$usersCollection->add($student1);
$usersCollection->add($student2);

$employee1 = new Employee();
$employee1->setName('Brian');
$employee1->setSalary(300);
$employee2 = new Employee();
$employee2->setName('George');
$employee2->setSalary(400);
$usersCollection->add($employee1);
$usersCollection->add($employee2);

echo 'Сумма стипендий: <br>';
echo $usersCollection->getTotalScholarship(). '<br>';
echo 'Сумма зарплат: <br>';
echo $usersCollection->getTotalSalary(). '<br>';
echo 'Общая сумма зарплат и стипендий: <br>';
echo $usersCollection->getTotalPayment(). '<br>';


// Exercise 28-2 - Создайте несколько объектов класса Post: программист, менеджер водитель.

$post1 = new Post('программист', 500);
$post2 = new Post('менеджер', 800);
$post3 = new Post('водитель', 600);

// Exercise 28-7 - Создайте объект класса Employee с должностью программист.
// При его создании используйте один из объектов класса Post, созданный нами ранее.

$employee = new Employee28('Edward', 'Green', $post1);

// Exercise 28-8 - Выведите на экран имя, фамилию, должность и зарплату созданного работника.
echo 'Имя работника: <br>';
echo $employee->getName() . '<br>';
echo 'Фамилия работника: <br>';
echo $employee->getSurname() . '<br>';
echo 'Должность работника: <br>';
echo $employee->getPost()->getPost() . '<br>';
echo 'Зарплата работника: <br>';
echo $employee->getPost()->getSalary() . '<br>';

// Exercise 28-9 - Реализуйте в классе Employee28 метод changePost, который будет изменять должность работника
// на другую. Метод должен принимать параметром объект класса Post.
// Укажите в методе тип принимаемого параметра в явном виде.
$employee->changePost($post2);
echo 'Должность работника: <br>';
echo $employee->getPost()->getPost() . '<br>';

// Exercise 29-2 - Пусть дан массив с числами. Найдите с помощью класса ArraySumHelper сумму квадратов
// элементов этого массива.
$arr = range(1, 3);
echo ArraySumHelper::getSum2($arr) . '<br>';

// Exercise 30-1 - Сделайте класс Num, у которого будут два публичных статических свойства: num1 и num2.
// Запишите в первое свойство число 2, а во второе - число 3. Выведите сумму значений свойств на экран.
Num::$num1 = 2;
Num::$num2 = 3;
echo Num::$num1 + Num::$num2 . '<br>';


// Exercise 30-2 - Сделайте класс Num, у которого будут два приватны статических свойства: num1 и num2.
// Пусть по умолчанию в свойстве num1 хранится число 2, а в свойстве num2 - число 3.
echo Num::$num1 + Num::$num2 . '<br>';

// Exercise 30-3 - Сделайте в классе Num метод getSum, который будет выводить на экран
// сумму значений свойств num1 и num2.
echo Num::getSum() . '<br>';

// Exercise 30-4 - Добавьте в наш класс Geometry метод, который будет находить объем шара по радиусу.
// С помощью этого метода выведите на экран объем шара с радиусом 10.
echo Geometry::getSphereVolume(10) . '<br>';

// Exercise 31-2 - Самостоятельно переделайте код вашего класса User31 в соответствии с теоретической частью урока.
echo User31::getCount() . '<br>';
$user31 = new User31('Jason');
echo User31::getCount() . '<br>';

// Exercise 32-2 - С помощью написанного класса Circle найдите длину окружности и площадь круга с радиусом 10.
$circle = new Circle(10);
echo $circle->getCircuit() . '<br>';
echo $circle->getSquare() . '<br>';

// Exercise 33-1 - Сделайте объект какого-нибудь класса. Примените к объекту функцию get_class и узнайте имя класса,
// которому принадлежит объект.
$employee = new Employee();
echo "Имя класса: " , get_class($employee) . '<br>';

// Exercise 33-2 - Сделайте два класса: Test1 и Test2. Пусть оба класса имеют свойство name.
// Создайте некоторое количество объектов этих классов и запишите в массив $arr в произвольном порядке.
// Переберите этот массив циклом и для каждого объекта выведите значение его свойства name и имя класса,
// которому принадлежит объект.

$test1_1 = new Test1();
$test1_1->name = 'Jeffrey';
$test1_2 = new Test1();
$test1_2->name = 'Ryan';

$test2_1 = new Test2();
$test2_1->name = 'Jacob';
$test2_2 = new Test2();
$test2_2->name = 'Gary';
$arr = [];
$arr[] = $test2_1;
$arr[] = $test1_2;
$arr[] = $test2_2;
$arr[] = $test1_1;

foreach ($arr as $object)
{
    echo $object->name . ' Имя класса: ' . get_class($object) . '<br>';
}

// Exercise 33-3 - Сделайте класс Test с методами method1, method2 и method3.
// С помощью функции get_class_methods получите массив названий методов класса Test.

$testMethods = get_class_methods('Test');
foreach ($testMethods as $method)
{
    echo $method . '<br>';
}

// Exercise 33-4 - Создайте объект класса Test, запишите его в переменную $test. С помощью функции get_class_methods
// получите массив названий методов объекта. Переберите его циклом и в цикле вызовите каждый метод класса,
// используя объект $test. Переберите этот массив циклом и в этом цикле вызовите каждый метод объекта.

$test = new Test();
$testMethods = get_class_methods($test);
foreach ($testMethods as $method)
{
   echo $test->$method() . '<br>';
}


// Exercise 33-5 - Сделайте класс Test с публичными свойствами prop1 и prop2,
// а также с приватными свойствами prop3 и prop4.
// Exercise 33-6 - Вызовите функцию get_class_vars снаружи класса Test. Выведите массив доступных свойств.

$test = new Test();
$testVars = get_class_vars( get_class($test));
foreach ($testVars as $name => $value)
{
    echo $name . '<br>';
}

// Exercise 33-7 - Вызовите функцию get_class_vars внутри класса Test (например, в конструкторе).
// Выведите массив доступных свойств.


// Exercise 33-8 - Сделайте класс Test с публичными свойствами prop1 и prop2, а также с приватными
// свойствами prop3 и prop4. Создайте объект этого класса. С помощью функции get_object_vars
// получите массив свойств созданного объекта.

$test = new Test();
$testVars = get_object_vars($test);
echo "Свойства объекта: <br>";
foreach ($testVars as $name => $value)
{
    echo $name . '<br>';
}


// Exercise 33-9 - Пусть у вас есть класс Test1 и нет класса Test3. Проверьте, что выведет функция
// class_exists для класса Test1 и для класса Test2.
var_dump(class_exists('Test1'));
echo '<br>';
var_dump(class_exists('Test3'));
echo '<br>';

// Exercise 33-11 - Сделайте класс Test с методом method1 и без метода method2.
// Проверьте, что выведет функция method_exists для метода method1 и для метода method2.
echo "Метод - method1 в Классе: Test <br>";
var_dump(method_exists($test, 'method1'));
echo "<br> Метод - method4 в Классе: Test <br>";
var_dump(method_exists('Test', 'method4'));
echo '<br>';


// Exercise 33-12 - Проверьте, существует ли такой класс. Если существует - проверьте существование
// переданного метода. Если и метод существует - создайте объект данного класса,
// вызовите указанный метод и выведите результат его работы на экран.

if (class_exists('Test')) {
    if (method_exists('Test', 'method1')) {
        $test = new Test();
        echo $test->method1() . '<br>';
    }
}


// Exercise 33-13 - Сделайте класс Test3 со свойством prop1 и без свойства prop2.
// Проверьте, что выведет функция property_exists для свойства prop1 и для свойства prop2.
var_dump(property_exists('Test3', 'prop1'));
echo '<br>';
var_dump(property_exists('Test3', 'prop2'));
echo '<br>';

// Exercise 33-14 - Дан массив со свойствами класса. Дан также класс, имеющий часть из этих свойств.
// Переберите этот массив циклом, для каждого свойства проверьте, существует ли оно в классе и,
// если существует, выведите на экран значение этого свойства.
$arr  = ['prop0', 'prop1', 'prop2', 'prop3','prop4', 'prop5'];
$test3 = new Test3();
foreach ($arr as $property){
    if (property_exists($test3, $property)) {
        echo $test3->$property . '<br>';
    }
}

// Exercise 33-15 - Сделайте класс Employee наследующий от User. С помощью функции
// get_parent_class выведите на экран родителя класса User.
 $employee = new Employee();
echo get_parent_class($employee) . '<br>';


// Exercise 33-16 - Сделайте класс ChildClass(Driver) наследующий от ParentClass(Employee), который в свою
// очередь наследует от GrandParentClass(User)

// Exercise 33-17 - С помощью функции is_subclass_of проверьте, является ли класс ChildClass
// потомком GrandParentClass.
echo is_subclass_of('Driver', 'User') . '<br>';

// Exercise 33-18 - С помощью функции is_subclass_of проверьте, является ли класс ParentClass
// потомком GrandParentClass.
echo is_subclass_of('Employee', 'User') . '<br>';

// Exercise 33-19 - С помощью функции is_subclass_of проверьте, является ли класс ChildClass потомком ParentClass.
echo is_subclass_of('Driver', 'Employee') . '<br>';

// Exercise 33-20 - Сделайте класс ChildClass(Driver) наследующий от ParentClass(Employee).
// Создайте объект класса ChildClass(Driver), запишите его в переменную $obj.
$obj = new Driver();

// Exercise 33-21 - С помощью функции is_a проверьте, принадлежит ли объект $obj классу ChildClass(Driver).
echo is_a($obj, 'Driver') . '<br>';

// Exercise 33-22 - С помощью функции is_a проверьте, принадлежит ли объект $obj классу ParentClass.
echo is_a($obj, 'Employee') . '<br>';

// Exercise 33-23 - Выведите на экран список всех объявленных классов.
/*foreach (get_declared_classes() as $class){
        echo $class . '<br>';
}*/


// Exercise 35-1 - Самостоятельно реализуйте такой же абстрактный класс User35,
// а также классы Employee35 и Student35, наследующие от него.

// Exercise 35-2 - Добавьте в ваш класс User такой же абстрактный метод increaseRevenue.
// Напишите реализацию этого метода в классах Employee и Student..
$employee35 = new Employee35;
$employee35->setName('Eric');
$employee35->setSalary(500);
$employee35->increaseRevenue(100);
echo $employee35->getSalary() . '<br>';

$student35= new Student35();
$student35->setName('Jonathan');
$student35->setScholarship(200);
$student35->increaseRevenue(100);
echo $student35->getScholarship() . '<br>';


// Exercise 35-3 - Добавьте также в ваш класс User абстрактный метод decreaseRevenue (уменьшить зарплату).
// Напишите реализацию этого метода в классах Employee и Student.
$employee35->decreaseRevenue(200);
echo $employee35->getSalary() . '<br>';

$student35->decreaseRevenue(100);
echo $student35->getScholarship() . '<br>';

// Exercise 35-4 - Сделайте аналогичный класс Rectangle (прямоугольник), у которого будет два приватных
// свойства: $a для ширины и $b для длины. Данный класс также должен наследовать от класса Figure
// и реализовывать его методы..
$rectangle = new Rectangle(4, 2);
echo $rectangle->getSquare() . '<br>';
echo $rectangle->getPerimeter() . '<br>';


// Exercise 35-5 - Добавьте в класс Figure метод getSquarePerimeterSum, который будет находить
// сумму площади и периметра.
echo $rectangle->getSquarePerimeterSum() . '<br>';

// Exercise 36-1 - Сделайте класс Disk (круг), реализующий интерфейс Figure.
$disk = new Disk(4);
echo $disk->getSquare() . '<br>';
echo $disk->getPerimeter() . '<br>';

// Exercise 37-1 - Реализуйте такой же класс FiguresCollection.
// Exercise 37-2 - Добавьте в класс FiguresCollection метод getTotalPerimeter
// для нахождения суммарного периметра всех фигур..

// Exercise 38-1 - Сделайте класс User38, который будет реализовывать iUser интерфейс.

// Exercise 39-1 - Сделайте интерфейс iCube, который будет описывать фигуру Куб.
// Пусть ваш интерфейс описывает конструктор, параметром принимающий сторону куба, а также
// методы для получения объема куба и площади поверхности.

// Exercise 39-2 - Сделайте класс Cube, реализующий интерфейс iCube.

// Exercise 39-3 - Сделайте интерфейс iUser, который будет описывать юзера. Предполагается, что у юзера будет
// имя и возраст и эти поля будут передаваться параметрами конструктора. Пусть ваш интерфейс также задает то,
// что у юзера будут геттеры (но не сеттеры) для имени и возраста.

// Exercise 39-4 - Сделайте класс User38, реализующий интерфейс iUser.

// Exercise 40-1 - Сделайте интерфейс iUser с методами getName, setName, getAge, setAge.

// Exercise 40-2 - Сделайте интерфейс iEmployee40, наследующий от интерфейса iUser40 и добавляющий в него
// методы getSalary и setSalary.

// Exercise 40-2 - Сделайте класс Employee40, реализующий интерфейс iEmployee40.

// Exercise 41-1 - Сделайте интерфейс Figure3d (трехмерная фигура), который будет иметь
// метод getVolume (получить объем) и метод getSurfaceSquare (получить площадь поверхности).

// Exercise 41-2 - Сделайте класс Cube41, который будет реализовывать интерфейс iFigure3d.

// Exercise 41-3 - Создайте несколько объектов класса Quadrate, несколько объектов класса Rectangle
// и несколько объектов класса Cube41. Запишите их в массив $arr в случайном порядке.

$quadrate1 = new Quadrate(2);
$quadrate2 = new Quadrate(3);

$rectangle1 = new Rectangle(3, 2);
$rectangle2 = new Rectangle(4, 2);

$cube1 = new Cube41(2);
$cube2 = new Cube41(3);

$arr = [];
$arr[] = $cube1;
$arr[] = $rectangle2;
$arr[] = $quadrate1;
$arr[] = $quadrate2;
$arr[] = $cube2;
$arr[] = $rectangle1;


// Exercise 41-4 - Переберите циклом массив $arr и выведите на экран только площади объектов
// реализующих интерфейс iFigure.
echo "Площади объектов реализующих интерфейс iFigure: " . '<br>';
foreach ($arr as $object){
    if ($object instanceof iFigure) {
        echo $object->getSquare() . '<br>';
    }
}


// Exercise 41-5 - Переберите циклом массив $arr и выведите для плоских фигур их площади,
// а для объемных - площади их поверхности.
foreach ($arr as $object){
    if ($object instanceof iFigure) {
        echo $object->getSquare() . '<br>';
    }

    if ($object instanceof iFigure3d) {
        echo $object->getSurfaceSquare() . '<br>';
    }
}



// Exercise 42-1 - Сделайте так, чтобы класс Rectangle42 также реализовывал два интерфейса: и iFigure, и iTetragon.

// Exercise 42-2 - Сделайте интерфейс iCircle с методами getRadius и getDiameter.

// Exercise 42-3 - Сделайте так, чтобы класс Disk42 также реализовывал два интерфейса: и iFigure, и iCircle.

// Exercise 43-1 - Скопируйте код моего класса Employee и код интерфейса iProgrammer.
// Не копируйте мою заготовку класса Programmer - не подсматривая в мой код реализуйте этот класс самостоятельно.

// Exercise 44-1 - Сделайте класс Sphere, который будет реализовывать интерфейс iSphere.

// Exercise 45-1 - Пусть у вас есть интерфейс iTest1 и нет интерфейса iTest2.
// Проверьте, что выведет функция interface_exists для интерфейса iTest1 и для интерфейса iTest2.
var_dump(interface_exists('iTest1'));
echo '<br>';
var_dump(interface_exists('iTest2'));
echo '<br>';

// Exercise 45-2 - Выведите на экран список всех объявленных интерфейсов.
/*foreach (get_declared_interfaces() as $interfac){
    echo $interfac . '<br>';
}*/

// Exercise 46-1 - Реализуйте класс Country со свойствами name, age, population и геттерами для них.
// Пусть наш класс для сокращения своего кода использует уже созданный нами трейт Helper.
$country = new Country('Ukraine', 30, 46);
echo $country->getName() . '<br>';
echo $country->getAge() . '<br>';
echo $country->getPopulation() . '<br>';

// Exercise 46-2 - Сделайте 3 трейта с названиями Trait1, Trait2 и Trait3. Пусть в первом трейте будет метод method1,
// возвращающий 1, во втором трейте - метод method2, возвращающий 2, а в третьем трейте - метод method3,
// возвращающий 3. Пусть все эти методы будут приватными.

// Exercise 46-3 - Сделайте класс Test46, использующий все три созданных нами трейта.
// Сделайте в этом классе публичный метод getSum, возвращающий сумму результатов методов подключенных трейтов.
$test46 = new Test46();
echo $test46->getSum() . '<br>';

// Exercise 47-1 - Сделайте 3 трейта с названиями Trait1, Trait2 и Trait3.
// Пусть в первом трейте будет метод method, возвращающий 1, во втором трейте - одноименный метод,
// возвращающий 2, а в третьем трейте - одноименный метод, возвращающий 3.

// Exercise 47-2 - Сделайте класс Test, использующий все три созданных нами трейта.
// Сделайте в этом классе метод getSum2, возвращающий сумму результатов методов подключенных трейтов.
echo $test46->getSum2() . '<br>';

// Exercise 50-1 - Скопируйте код моего трейта TestTrait и код моего класса Test. Удалите из класса метод method2.
// Убедитесь в том, что отсутствие его реализации приведет к ошибке PHP.

// Exercise 51-1 - Самостоятельно сделайте такие же трейты, как у меня и подключите их к классу Test.
// Сделайте в этом классе метод getSum, возвращающий сумму результатов методов подключенных трейтов.
$test51 = new Test51();
echo $test51->getSum() . '<br>';

// Exercise 51-1 - Пусть у вас есть трейт Trait1 и нет трейта Trait5.
// Проверьте, что выведет функция trait_exists для трейта Trait1 и для трейта Trait5.
var_dump(trait_exists('Trait1'));
echo '<br>';
var_dump(trait_exists('Trait5'));
echo '<br>';

// Exercise 51-2 - Выведите на экран список всех объявленных трейтов.
foreach (get_declared_traits() as $trait){
    echo $trait . '<br>';
}